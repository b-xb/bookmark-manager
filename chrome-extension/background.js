function clickHandler(info) {
    console.log("hello");

    var submitUrl = "https://bencrosbie.me.uk/bookmarkmanager/add/?";

    if (info.selectionText) {
        submitUrl += "title=" + encodeURIComponent(info.selectionText) + "&";
    }


    if (info.linkUrl) {
        submitUrl += "url=" + encodeURIComponent(info.linkUrl);
    }
    
    console.log(submitUrl);
    openBookmarklet(submitUrl);
};



function openBookmarklet(submitUrl){
    var a=window,
        b=document,
        c=encodeURIComponent,
        d=a.open(submitUrl,
                 "bkmk_popup",
                 "left="+((a.screenX||a.screenLeft)+10)+",top="+((a.screenY||a.screenTop)+10)+",height=420px,width=550px,resizable=1,alwaysRaised=1"
        );
        a.setTimeout(
            function(){
                d.focus()
            },
            300
        )
};

chrome.contextMenus.create({
    "title": "Boomark This Link",
    "contexts": ["link"],
    "onclick" : clickHandler
});