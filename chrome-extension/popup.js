DJANGO_API_KEY = "ADD_API_KEY_HERE";

function toggleTagEditor(){
    $(".tag-editor").toggle();
}
 
function toggleURLDisplay(){
    $(".url-display").toggle();
}

function selectAll(){
    $(".tabToSave").prop("checked", true);
}

function deselectAll(){
    $(".tabToSave").prop("checked", false);
}
  
function thisWindowTabs(){
        //var myarray = new Array();
        
        
        chrome.tabs.query(
            {
                windowId: chrome.windows.WINDOW_ID_CURRENT
            },
            
            function(tabs){
                requestTabs(tabs);
            }
        );

};

function thisDeviceTabs(selectedSession){
    chrome.windows.get(
        $(selectedSession).data("windowId"), 
        {
            populate:true
        },
        function(window){
            requestTabs(window.tabs)
        }
    );
}

function otherDeviceTabs(selectedSession){
    chrome.sessions.getDevices(function(devices){
        devices.forEach(function(device){
            if (device.deviceName == $(selectedSession).data("deviceName")) {
                sessions = device.sessions;
                sessions.forEach(function(session){
                    if (session.window.sessionId == $(selectedSession).data("sessionId")) {
                        requestTabs(session.window.tabs);
                    }
                });
            }
        });
    })
}

function recentlyClosedTabs(selectedSession){
    console.log(selectedSession);

    chrome.sessions.getRecentlyClosed(
        function(sessions) {
            sessions.forEach(
                function(session, index){
                    if (session.lastModified === 0) {
                        if (session.window.sessionId === $(selectedSession).data("sessionId")) {
                            requestTabs(session.window.tabs);
                        }
                    }
                }
            );
        }
    );
}

                
function requestTabs(tabs) {
    $("body").data("tabsdata",tabs);
    
    form = document.getElementById('bookmarks');
    
    $("body").data("tabsdata").forEach(function(tab) {
    
        var mydiv = document.createElement('div');
        $(mydiv).data("tabdata",tab);
        $(mydiv).attr( "id", "bookmark-"+$(mydiv).data("tabdata").id );
        $(mydiv).addClass('bookmark');
        mydiv.innerHTML=
            "<input class='tabToSave' type='checkbox' name='tabToSave' id='select-"+
                $(mydiv).data("tabdata").id+
            "' checked>"+
            "<a href='"+
                $(mydiv).data("tabdata").url+
            "' title='"+
                $(mydiv).data("tabdata").url+
            "'>"+
                $(mydiv).data("tabdata").title+
            "</a>"+
            "<br/>"+
            "<span class='url-display' style='display: none;'>"+
                $(mydiv).data("tabdata").url+
            "<br/>"+
            "</span>"+
            "<input class='tag-editor' type='text' name='tags' size='100' >";
        form.appendChild(mydiv);
        
    });
    
    addSelectedLinksCountListener();
}


function submitForm(){

    function getLinks() {
        bookmarks = [];
        $('.bookmark .tabToSave:checked').each(function(i,checkbox){
            bookmark_element = checkbox.parentElement;
            bookmark = {
                "title": $(bookmark_element).data("tabdata").title,
                "url": $(bookmark_element).data("tabdata").url,
                "tags": $(bookmark_element).find('.tag-editor').val()
            };
            bookmarks.push(bookmark);
        });
        return bookmarks;
    }

    data = {
        "title": $('#session-title').val(),
        "tags": $('#session-tags').val(),
        "links": getLinks()
    };
    
    $.ajax({
        url: "https://bencrosbie.me.uk/sessionssaver/api/submit/",
        type: "POST",
        dataType: "json",
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(data),
        cache: false,
        beforeSend: function (xhr) {
            /////   Authorization header////////
            xhr.setRequestHeader("Authorization", "Token " + DJANGO_API_KEY);
        },
        success: function (returnData) {
            submitSuccess(returnData);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    }).fail(function () {
        $( "#results" ).text("Error")
    });

}

function submitSuccess(returnData){
    console.log(returnData);
    $( "#results" ).text(" " + returnData["bookmarks"].length + " Saved! ");
    $('<a/>', {
        href: "https://bencrosbie.me.uk/bookmarkmanager/?page=1&per_page=10&s="+returnData["pk"],
        text: "link"
    }).appendTo('#results');
}

function storeWindowInfo() {
    chrome.windows.getAll(
        function(windowObj, index){
            selectbox = document.getElementById('devices');
            windowObj.forEach(
                function(window, index){
                    deviceName = "this-device";
                    deviceDisplayName = "This Device";
                    var newDevice = document.createElement('option');
                    $(newDevice).data("deviceName",deviceName);
                    $(newDevice).data("windowId",window.id);
                    $(newDevice).attr("name",deviceName+" "+index);
                    $(newDevice).val(deviceName+" "+index);
                    newDevice.innerHTML = deviceDisplayName+": "+index;
                    selectbox.appendChild(newDevice);
                }
            )
        }
    )
}

function storeDeviceInfo() {
    chrome.sessions.getDevices(
        function(devices){
            selectbox = document.getElementById('devices');
            devices.forEach(
                function(device){
                    deviceName = device.deviceName
                    device.sessions.forEach(
                        function(session,index){
                            var newDevice = document.createElement('option');
                            $(newDevice).data("deviceName",deviceName);
                            $(newDevice).data("sessionId",session.window.sessionId);
                            $(newDevice).attr("name",deviceName+" "+index);
                            $(newDevice).val(deviceName+" "+index);
                            newDevice.innerHTML = deviceName+": "+index;
                            selectbox.appendChild(newDevice);
                        }
                    );
                }
            );
        
        }
    )
}

function storedRecentlyClosedInfo(){
    chrome.sessions.getRecentlyClosed(
        function(sessions) {
            selectbox = document.getElementById('devices');
            sessions.forEach(
                function(session, index){
                    if (session.lastModified === 0) {
                        deviceName = "recently-closed";
                        deviceDisplayName = "Recently Closed";
                        var newDevice = document.createElement('option');
                        $(newDevice).data("deviceName",deviceName);
                        $(newDevice).data("sessionId",session.window.sessionId);
                        $(newDevice).attr("name",deviceName+" "+index);
                        $(newDevice).val(deviceName+" "+index);
                        newDevice.innerHTML = deviceDisplayName+": "+index;
                        selectbox.appendChild(newDevice);
                    } 
                }
            );
        }
    );
}

function deviceSessionSelectListener(){
    $( "#devices" )
        .change(function () {
            $('#bookmarks').empty();
                        
            $( "#devices option:selected" ).each(function() {
                if ($( this ).val() == "this-window") {
                    thisWindowTabs();
                } else if ($( this ).data("deviceName") == "this-device") {
                    thisDeviceTabs($( this ));
                } else if ($( this ).data("deviceName") == "recently-closed") {
                    recentlyClosedTabs($( this ));
                } else {
                    otherDeviceTabs($( this ));
                }
                
            });
            
        })
}

function addSelectedLinksCountListener(){
    $( "#selected-count" ).text($('.bookmark .tabToSave:checked').length + " Selected");
    $( ".tabToSave" )
        .change(function () {
            $( "#selected-count" ).text($('.bookmark .tabToSave:checked').length + " Selected");
        })
}

// Runs script as soon as the document's DOM is ready.
document.addEventListener('DOMContentLoaded', function () {
    thisWindowTabs();
    storeWindowInfo();
    storeDeviceInfo();
    storedRecentlyClosedInfo();
    deviceSessionSelectListener();
    document.getElementById('tag-editor-toggle').addEventListener('click', toggleTagEditor);
    document.getElementById('url-display-toggle').addEventListener('click', toggleURLDisplay);
    document.getElementById('select-all-button').addEventListener('click', selectAll);
    document.getElementById('deselect-all-button').addEventListener('click', deselectAll);
    document.getElementById('save-selected').addEventListener('click', submitForm);
});
