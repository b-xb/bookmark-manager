package uk.me.bencrosbie.testapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class JsonPostActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_post);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MyActivity.EXTRA_MESSAGE);

        textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText(message);
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.content);
        layout.addView(textView);

        new PostJSON().execute(message);

    }


    private class PostJSON extends AsyncTask<String, Void, String> {


        String title = "";

        protected String doInBackground(String... inputMessage) {

            String message = "{\"title\":\"Title\",\"tags\":\"tag1,tag2\",\"links\":[]}";


            try {

                URL url = new URL("https://bencrosbie.me.uk/sessionssaver/api/submit/");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Authorization", "Token " + R.string.django_api_key);

                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(message);
                writer.close();

                if (connection.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
                    // OK
                    title = "Saved."+connection.getContentType();


                } else {
                    // Server returned HTTP error code.
                    title = "Not Working. "+ connection.getResponseCode() + " " + connection.getContentType() + " " + connection.getResponseMessage();

                }
            } catch (MalformedURLException e) {
                // ...
                title = "MalformedURLException "+ e;
            } catch (IOException e) {
                // ...
                title = "IOException "+ e;
            }



            return title;
        }


        protected void onPostExecute(String url) {

            textView.setText(title);
        }
    }

}
