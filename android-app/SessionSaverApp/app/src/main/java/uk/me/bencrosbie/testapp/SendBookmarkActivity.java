package uk.me.bencrosbie.testapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SendBookmarkActivity extends AppCompatActivity {

    TextView responseTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_bookmark);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        responseTextView = new TextView(this);
        responseTextView.setTextSize(40);
        responseTextView.setText("Saving...");
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.content);
        layout.addView(responseTextView);

        Intent intent = getIntent();

        String title_string = intent.getStringExtra(BookmarkActivity.TITLE_STRING);
        String url_string = intent.getStringExtra(BookmarkActivity.URL_STRING);
        String tags_string = intent.getStringExtra(BookmarkActivity.TAGS_STRING);

        new PostJSON().execute(title_string,url_string,tags_string);

    }

    private class PostJSON extends AsyncTask<String, Void, String> {

        //String response = "";

        protected String doInBackground(String... inputStrings) {

            String data = null;
            String response = "";

            //TODO: using a library to make the JSON would be an improvement

            data = "{\"title\":\"Android Nexus link\",\"tags\":\"androidlink\",\"links\":[" +
                        "{" +
                            "\"title\": \"" + escapeQuotes(inputStrings[0]) + "\"," +
                            "\"url\": \"" + escapeQuotes(inputStrings[1]) + "\"," +
                            "\"tags\": \"" + escapeQuotes(inputStrings[2]) + "\"" +
                        "}" +
                    "]}";


            try {

                URL url = new URL("https://bencrosbie.me.uk/sessionssaver/api/submit/");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Authorization", "Token " +  R.string.django_api_key);

                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(data);
                writer.close();

                if (connection.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
                    // OK
                    response = "Saved."+connection.getContentType();

                } else {
                    // Server returned HTTP error code.
                    response = "Not Working. "+
                               connection.getResponseCode() + " " +
                               connection.getContentType() + " " +
                               connection.getResponseMessage() + " " +
                               data;
                }

            } catch (MalformedURLException e) {
                // ...
                response = "MalformedURLException "+ e;
            } catch (IOException e) {
                // ...
                response = "IOException "+ e;
            }

            return response;
        }


        protected void onPostExecute(String response) {

            responseTextView.setText(response);
        }

        private String escapeQuotes(String str){
            String newStr = str.replaceAll("\"","\\\"");
            return newStr;
        }
    }

}
