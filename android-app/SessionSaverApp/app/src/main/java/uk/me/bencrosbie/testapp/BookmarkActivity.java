package uk.me.bencrosbie.testapp;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class BookmarkActivity extends AppCompatActivity {

    EditText bookmark_url;
    EditText bookmark_title;
    EditText bookmark_tags;

    public final static String URL_STRING = "com.mycompany.myfirstapp.URL_STRING";
    public final static String TITLE_STRING = "com.mycompany.myfirstapp.TITLE_STRING";
    public final static String TAGS_STRING = "com.mycompany.myfirstapp.TAGS_STRING";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        bookmark_url = (EditText) findViewById(R.id.bookmark_url);
        bookmark_title = (EditText) findViewById(R.id.bookmark_title);
        bookmark_tags = (EditText) findViewById(R.id.bookmark_tags);


        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();



        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendIntent(intent); // Handle text being sent
            }
        } else if (Intent.ACTION_VIEW.equals(action)) {

            handleViewIntent(intent);
        } else {
            handleAppIntent(intent);
        }


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    void handleSendIntent(Intent intent) {


        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);

        if (sharedText != null) {


            //extract url from sharedText

            String url = sharedText.substring(sharedText.lastIndexOf(" ") + 1);

            bookmark_url.setText(url);

            if (sharedText.lastIndexOf(" ") != -1) {

                String title = sharedText.substring(0, sharedText.lastIndexOf(" "));
                bookmark_title.setText(title);


            } else {

                new FetchUrlInfo().execute(url);

            }
        }
    }

    void handleViewIntent(Intent intent) {

        String url = intent.getData().toString();

        if (url != null) {

            bookmark_url.setText(url);

            new FetchUrlInfo().execute(url);
        }
    }

    void handleAppIntent(Intent intent) {

        String url = intent.getStringExtra(MyActivity.EXTRA_URL);

        if (url != null) {

            bookmark_url.setText(url);

            new FetchUrlInfo().execute(url);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Bookmark Page",
                Uri.parse("http://host/path"),
                Uri.parse("android-app://uk.me.bencrosbie.testapp/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Bookmark Page",
                Uri.parse("http://host/path"),
                Uri.parse("android-app://uk.me.bencrosbie.testapp/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    private class FetchUrlInfo extends AsyncTask<String, Void, String> {
        String title = "";

        protected String doInBackground(String... url) {

            try {
                Document doc = Jsoup.connect(url[0]).get();
                title = doc.title();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return title;
        }


        protected void onPostExecute(String url) {
            bookmark_title.setText(title);
        }
    }

    /**
     * Called when the user clicks the Send button
     */
    public void sendBookmark(View view) {
        //TODO: Do something in response to button
        Intent intent = new Intent(this, SendBookmarkActivity.class);

        String title_string = bookmark_title.getText().toString();
        intent.putExtra(TITLE_STRING, title_string);

        String url_string = bookmark_url.getText().toString();
        intent.putExtra(URL_STRING, url_string);

        String tags_string = bookmark_tags.getText().toString();
        intent.putExtra(TAGS_STRING, tags_string);

        startActivity(intent);
    }

}
