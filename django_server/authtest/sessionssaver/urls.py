from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from sessionssaver import views

urlpatterns = [
    url(r'^api/submit/$', views.submit_form),
    url(r'^api/bookmarks/$', views.get_bookmarks),
    url(r'^api/sessions/$', views.get_sessions),
    #url(r'^tags/$', views.get_tags),
]

urlpatterns = format_suffix_patterns(urlpatterns)
