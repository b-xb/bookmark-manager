#from django.shortcuts import render

# Create your views here.

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response

from taggit.managers import TaggableManager
from taggit.utils import parse_tags
from taggit.models import TaggedItem

from sessionssaver.models import Session, Bookmark
from sessionssaver.serializers import SessionSerializer, BookmarkSerializer  #, TaggedItemSerializer



@api_view(['POST','GET'])
#@authentication_classes((SessionAuthentication,))
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def submit_form(request):
    """
    submit the form 
    """
    if request.method == 'GET':
        sessions = Session.objects.all()
        serializer = SessionSerializer(sessions, many=True)
        response = Response(serializer.data)
        return response

    if request.method == 'POST':

        serializer = SessionSerializer(data=request.data)

        if serializer.is_valid():

            session_object = serializer.save()

            #get tags
            tags_data = serializer.initial_data.pop('tags')
            tags = parse_tags(tags_data)
            for tag in tags:
                session_object.tags.add(tag)

            session_object.save()


            #get bookmarks
            bookmarks_data = serializer.initial_data.pop('links')
            for bookmark_data in bookmarks_data:
                bookmark_data["session"] = session_object.pk
                bookmark_serializer = BookmarkSerializer(data=bookmark_data)
                if bookmark_serializer.is_valid():
                    bookmark_object = bookmark_serializer.save()
                   
                    #get tags
                    tags_data = bookmark_serializer.initial_data.pop('tags')
                    tags = parse_tags(tags_data)
                    for tag in tags:
                        bookmark_object.tags.add(tag)

                    bookmark_object.save()
                    #return Response(bookmark_data, status=status.HTTP_201_CREATED)
                else:
                    return Response(bookmark_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST','GET'])
@authentication_classes((SessionAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def get_bookmarks(request):
    """
    GET view all bookmarks
    POST view selected bookmark by ID (TODO)
    """
    if request.method == 'GET':
        bookmarks = Bookmark.objects.all()
        serializer = BookmarkSerializer(bookmarks, many=True)
        response = Response(serializer.data)
        return response

"""
@api_view(['POST','GET'])
@authentication_classes((SessionAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def get_tags(request):
    if request.method == 'GET':
        tags = TaggedItem.objects.all()
        serializer = TaggedItemSerializer(tags, many=True)
        response = Response(serializer.data)
        return response
"""

@api_view(['POST','GET'])
@authentication_classes((SessionAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def get_sessions(request):
    """
    GET view all sessions
    POST view selected session by ID (TODO)
    """
    if request.method == 'GET':
        sessions = Session.objects.all()
        serializer = SessionSerializer(sessions, many=True)
        response = Response(serializer.data)
        return response
