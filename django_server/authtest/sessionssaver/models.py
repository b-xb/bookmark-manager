from django.db import models
from django.contrib.auth.models import User

from taggit.managers import TaggableManager 
from taggit.models import Tag

from django.db.models.query import QuerySet
from model_utils.managers import PassThroughManager

from sessionssaver.fields import SessionsSaverURLField

# Create your models here.


class TestModel(models.Model):
    url = SessionsSaverURLField(max_length=1000)



class Session(models.Model):
    datetime_added = models.DateTimeField(auto_now_add=True)
    date_added = models.DateField(auto_now_add=True)
    title = models.CharField(max_length=500,blank=True,null=True)
    notes = models.TextField(blank=True,null=True)
    tags = TaggableManager(blank=True)
    owner = models.ForeignKey(User, default=1, editable=False)
    def owned_by(self,user):
        return self.owner == user
    def __unicode__(self):  # Python 3: def __str__(self):
        return self.title if self.title else str(self.datetime_added)

class Folder(models.Model):
    title = models.CharField(max_length=500)
    notes = models.TextField(blank=True,null=True)
    owner = models.ForeignKey(User, default=1, editable=False)
    parent_folder = models.ForeignKey('self', related_name="subfolders", blank=True, null=True)
    def path(self):
        if self.parent_folder:
            path_list = self.parent_folder.path()
            path_list.append(self)
            return path_list
        else:
            return [self]
    def owned_by(self,user):
        return self.owner == user
    def __unicode__(self):  # Python 3: def __str__(self):
        return (str(self.parent_folder) + " > " if self.parent_folder else "") + self.title

class Bookmark(models.Model):
    datetime_added = models.DateTimeField(auto_now_add=True)
    date_added = models.DateField(auto_now_add=True)
    title = models.CharField(max_length=1000)
    url = SessionsSaverURLField(max_length=1000)
    #url_backup = models.URLField(max_length=1000,default="http://default.com",blank=True,editable=False)
    notes = models.TextField(blank=True,null=True)
    tags = TaggableManager(blank=True)
    owner = models.ForeignKey(User, default=1, editable=False)
    session = models.ForeignKey(Session, related_name='bookmarks',blank=True,null=True)
    parent_folder = models.ForeignKey(Folder, related_name="bookmarks", blank=True, null=True)
    def owned_by(self,user):
        return self.owner == user    
    def __unicode__(self):  # Python 3: def __str__(self):
        return self.title



#class TagUserInfo
#    user = models.ForeignKey(User)
#    tag = models.ForeignKey(Tag)
#    bookmark_count = models.IntegerField(default=0)
#    session_count = models.IntegerField(default=0)
#
#    class Meta:
#        unique_together = ['user', 'tag']
#


# currently the SessionsSaverTag model is used in the tags view
# might use as replacement for core Taggit Tag model in the future, need to convert all current tags to this tag and extend other Tag stuff e.g. TaggableManager to work with this though
# current tags view not very elegant, alternative plan to make a model to store caches of bookmarks and sessions counts per user instead which might be more efficient, alternative model would be more efficient

#class SessionsSaverTagManager(models.Manager):
#    def filter_by_content_type_count(self,model):
#        pass

#class SessionsSaverTagQuerySet(QuerySet):
#    def key(self,tag):
#        return (tag.sessions(),tag.bookmarks())
#    def order_tags(self, *args, **kwargs):
#        qs = self.filter(*args, **kwargs)
#        return sorted(qs, key=self.key)



class SessionsSaverTag(Tag):
    #objects = PassThroughManager.for_queryset_class(SessionsSaverTagQuerySet)

    class Meta:
        proxy = True

    def bookmarks(self,whitelist_tags=[],hidden_tags=[]):
        hidden_tags = [hidden_tag for hidden_tag in hidden_tags if hidden_tag not in whitelist_tags]
        return Bookmark.objects \
                    .exclude(session__tags__slug__in=hidden_tags) \
                    .exclude(tags__slug__in=hidden_tags) \
                    .filter(tags__slug=self.slug)

        #return self.taggit_taggeditem_items \
        #       .filter(content_type__app_label="sessionssaver",content_type__model="bookmark") \
        #       .exclude(content_object__tags__slug__in=hidden_tags)

    def sessions(self,whitelist_tags=[],hidden_tags=[]):
        hidden_tags = [hidden_tag for hidden_tag in hidden_tags if hidden_tag not in whitelist_tags]
        return Session.objects \
                    .exclude(tags__slug__in=hidden_tags) \
                    .filter(tags__slug=self.slug)

        #return self.taggit_taggeditem_items \
        #       .filter(content_type__app_label="sessionssaver",content_type__model="session") \
        #       .exclude(object_id__in=hidden_tags)



