from django.db import models
from django import forms
from django.core import validators

import re

#allows south to recognise custom field
from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^sessionssaver\.fields\.SessionsSaverURLField"])

class SessionsSaverURLValidator(validators.URLValidator):
    regex = re.compile(
        r'^(?:file|chrome):/|'
        r'^(?:http|ftp)s?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}(?<!-)\.?)|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|'  # ...or ipv4
        r'\[?[A-F0-9]*:[A-F0-9:]+\]?)'  # ...or ipv6
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)

class SessionsSaverFormUrlField(forms.URLField):
    default_validators = [SessionsSaverURLValidator()]

class SessionsSaverURLField(models.URLField):
    default_validators = [SessionsSaverURLValidator()]
    def formfield(self, **kwargs):
        # As with CharField, this will cause URL validation to be performed
        # twice.
        defaults = {
            'form_class': SessionsSaverFormUrlField,
        }
        defaults.update(kwargs)
        return super(models.URLField, self).formfield(**defaults)
