from rest_framework import serializers
from sessionssaver.models import Session,Bookmark

from taggit.models import TaggedItem

from sessionssaver.fields import SessionsSaverURLValidator

class SessionSerializer(serializers.ModelSerializer):
    tags = serializers.PrimaryKeyRelatedField(many=True,read_only=True)
    bookmarks = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Session
        fields = ('pk','datetime_added','date_added','title','notes','tags','owner','bookmarks')


class BookmarkSerializer(serializers.ModelSerializer):
    tags = serializers.PrimaryKeyRelatedField(many=True,read_only=True)
    session = serializers.PrimaryKeyRelatedField(queryset=Session.objects.all())
    url = serializers.CharField(max_length=1000,validators=[SessionsSaverURLValidator()])

    class Meta:
        model = Bookmark 
        fields = ('pk','datetime_added','date_added','title','url','notes','owner','session','tags')

"""
class TaggedItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaggedItem
        fields = ('pk')
"""
