from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^', include('home.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('authtest.urls')),
    url(r'^tokentest/', include('tokentest.urls')),
    url(r'^submittest/', include('testmodel.urls')),
    url(r'^sessionssaver/', include('sessionssaver.urls')),
    url(r'^bookmarkmanager/', include('bookmarkmanager.urls', namespace='bookmarkmanager')),
    url(r'^lastfmscrobbler/', include('lastfmscrobbler.urls', namespace='lastfmscrobbler')),
    url(r'^youtubevidlist/', include('youtubevidlist.urls', namespace='youtubevidlist')),
    url(r'^vidlist/', include('youtubevidlist.urls', namespace='vidlist')),
    url(r'^bugtracker/', include('bugtracker.urls', namespace='bugtracker')),
)
