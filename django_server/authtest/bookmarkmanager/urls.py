from django.conf.urls import patterns, url

from bookmarkmanager import views

urlpatterns = patterns('',
    #url(r'^testpath$', views.testdomain),
    url(r'^$', views.index, name='index'),
    url(r'^sessions/$', views.sessions, name='sessions'),
    url(r'^tags/$', views.tags, name='tags'),
    url(r'^add/$', views.add_bookmark, name='add_bookmark'),
    url(r'^submit/$', views.submit_bookmark, name='submit_bookmark'),
    url(r'^submit/(?P<bookmark_id>\d+)/$', views.update_bookmark, name='update_bookmark'),
    url(r'^edit/(?P<bookmark_id>\d+)/$', views.edit_bookmark, name='edit_bookmark'),
    url(r'^view/(?P<bookmark_id>\d+)/$', views.view_bookmark, name='view_bookmark'),
    url(r'^delete/(?P<bookmark_id>\d+)/$', views.delete_bookmark, name='delete_bookmark'),
    url(r'^add/s/$', views.add_session, name='add_session'),
    url(r'^submit/s/$', views.submit_session, name='submit_session'),
    url(r'^submit/s/(?P<session_id>\d+)/$', views.update_session, name='update_session'),
    url(r'^edit/s/(?P<session_id>\d+)/$', views.edit_session, name='edit_session'),
    url(r'^delete/s/(?P<session_id>\d+)/$', views.delete_session, name='delete_session'),
    url(r'^add/f/$', views.add_folder, name='add_folder'),
    url(r'^submit/f/$', views.submit_folder, name='submit_folder'),
    url(r'^submit/f/(?P<folder_id>\d+)/$', views.update_folder, name='update_folder'),
    url(r'^edit/f/(?P<folder_id>\d+)/$', views.edit_folder, name='edit_folder'),
    url(r'^delete/f/(?P<folder_id>\d+)/$', views.delete_folder, name='delete_folder'),
    url(r'^login/$', 'django.contrib.auth.views.login', { 'template_name': 'bookmarkmanager/login.html'}, name='login'),
    url(r'^logout/$', views.bm_logout, name='bm_logout'),
)
