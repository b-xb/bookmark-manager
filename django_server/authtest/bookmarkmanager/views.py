from django.template import RequestContext, loader
from django.http import HttpResponse, HttpResponseRedirect

from sessionssaver.models import Bookmark, Session, SessionsSaverTag, Folder
from forms import BookmarkForm, SessionForm, FolderForm

from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import logout

from taggit.models import Tag
from django.core.exceptions import ObjectDoesNotExist #might use to detect if tags exist
from django.db.models import Count

import urlparse

def testdomain(request):
    return HttpResponse("path:"+request.path+" path_info:"+request.path_info)


@login_required()
def index(request):
    #fetch tags to filter by from GET request arguments
    if 'removetag' in request.GET and 'tags' in request.GET :
        current_tags = request.GET['tags']
        tags = current_tags.split(",");
        tags = [tag for tag in tags if tag != request.GET['removetag']]
        current_tags = ",".join(tags);
    elif 'tags' in request.GET:
        current_tags = request.GET['tags']
        tags = current_tags.split(",")
    else:
        current_tags = ''
        tags = []

    #exclude bookmarks with tags matching the following tags, unless the tag exists in the GET request
    hidden_tags = ["hidden"]
    hidden_tags = [hidden_tag for hidden_tag in hidden_tags if hidden_tag not in tags]

    base_url = request.get_host()
    
    if 'f' in request.GET and int(request.GET['f']) >= 0:
        if int(request.GET['f']) == 0:
            bookmarks = None;
            folders = Folder.objects.filter(parent_folder__isnull=True) 
            parent_folder = None;
            count = 0; 
        else:
            bookmarks = Bookmark.objects.filter(parent_folder_id=int(request.GET['f']))
            bookmarks = bookmarks.exclude(session__tags__slug__in=hidden_tags).exclude(tags__slug__in=hidden_tags)
            folders = Folder.objects.filter(parent_folder_id=int(request.GET['f'])) 
            parent_folder = Folder.objects.get(id=int(request.GET['f']));
            count = bookmarks.count() 
                     
    elif 's' in request.GET and int(request.GET['s']) >= 1:
        bookmarks = Bookmark.objects.exclude(tags__slug__in=hidden_tags)
        count = bookmarks.filter(session_id=int(request.GET['s'])).count()
        folders = None;
        parent_folder = None;
    else:
        bookmarks = Bookmark.objects.exclude(session__tags__slug__in=hidden_tags).exclude(tags__slug__in=hidden_tags)
        folders = None;
        parent_folder = None;

        for tag in tags:
            bookmarks = bookmarks.filter(tags__slug=tag)
        count = bookmarks.count() 


    if 'page' in request.GET and int(request.GET['page']) >= 1:
        page = int(request.GET['page'])
    else:
        page = 1
    if 'per_page' in request.GET and int(request.GET['per_page']) >= 1:
        per_page = int(request.GET['per_page'])
    else:
        per_page = 10

    page_start = ((page - 1) * per_page)
    page_end = min( page * per_page, count)

    if 'f' in request.GET and int(request.GET['f']) >= 0:
        session = None
        if 'f' in request.GET and int(request.GET['f']) >= 1:
            bookmarks = bookmarks.order_by('-datetime_added')[page_start:page_end]
    elif 's' in request.GET and int(request.GET['s']) >= 1:
        session = Session.objects.get(pk=int(request.GET['s']))
        bookmarks = bookmarks.filter(session_id=int(request.GET['s'])).order_by('-datetime_added')[page_start:page_end]
    else:
        session = None
        bookmarks = bookmarks.order_by('-datetime_added')[page_start:page_end]

    pager_arg_for_folder = ("&f=" + request.GET['f']) if 'f' in request.GET and int(request.GET['f']) >= 0 else ""
    pager_arg_for_session = ("&s=" + request.GET['s']) if not pager_arg_for_folder and 's' in request.GET and int(request.GET['s']) >= 1 else ""
    pager_arg_for_tags = "&tags="+current_tags if tags else ""
    num_pages = int(((count-1)/per_page) + 1)

    pager_first = ["<a href=\"" + reverse('bookmarkmanager:index') + "?page=1&per_page=" + str(per_page) + pager_arg_for_folder + pager_arg_for_session + pager_arg_for_tags + "\">1</a>"] if page != 1 else []
    pager_second = ["<a href=\"" + reverse('bookmarkmanager:index') + "?page=2&per_page=" + str(per_page) + pager_arg_for_folder + pager_arg_for_session + pager_arg_for_tags + "\">2</a>"] if page > 3 else []
    pager_dotsbefore = [".."] if page > 4 else [] 
    pager_prev = ["<a href=\"" + reverse('bookmarkmanager:index') + "?page="+str(page-1)+"&per_page=" + str(per_page) + pager_arg_for_folder + pager_arg_for_session + pager_arg_for_tags + "\">"+str(page-1)+"</a>"] if page > 2 else [] 
    pager_current = [str(page)]
    pager_next = ["<a href=\"" + reverse('bookmarkmanager:index') + "?page="+str(page+1)+"&per_page=" + str(per_page) + pager_arg_for_folder + pager_arg_for_session + pager_arg_for_tags + "\">"+str(page+1)+"</a>"] if page < num_pages - 1 else []
    pager_dotsafter = [".."] if page < num_pages - 3 else [] 
    pager_secondlast = ["<a href=\"" + reverse('bookmarkmanager:index') + "?page="+str(num_pages-1)+"&per_page=" + str(per_page) + pager_arg_for_folder + pager_arg_for_session + pager_arg_for_tags + "\">"+str(num_pages-1)+"</a>"] if page < num_pages - 2  else []
    pager_last = ["<a href=\"" + reverse('bookmarkmanager:index') + "?page="+str(num_pages)+"&per_page=" + str(per_page) + pager_arg_for_folder + pager_arg_for_session + pager_arg_for_tags + "\">"+str(num_pages)+"</a>"] if page < num_pages else []


    pager = pager_first + pager_second + pager_dotsbefore + pager_prev + pager_current + pager_next + pager_dotsafter + pager_secondlast +  pager_last










    #pager = map((lambda x: ("<a href=\"" + reverse('bookmarkmanager:index') + "?page=" + str(x) + "&per_page=" + str(per_page) + pager_arg_for_folder + pager_arg_for_session + pager_arg_for_tags +"\">"+str(x)+"</a>") if x != page else str(x) ),list(range(1, int((count-1)/per_page) + 2, 1)))
    offsets = map((lambda x: ("<a href=\"" + reverse('bookmarkmanager:index') + "?page=1&per_page=" + str(x) + pager_arg_for_folder + pager_arg_for_session + pager_arg_for_tags + "\">"+str(x)+"</a>") if x != per_page else str(x) ),[10,25,50,100])

    #if (page_end > count):
    #    page_end = count
    if 'f' in request.GET and int(request.GET['f']) >= 0:
        template = loader.get_template('bookmarkmanager/folders.html')
    else:
        template = loader.get_template('bookmarkmanager/index.html')
    context = RequestContext(request, {
        'page_start': (page_start + 1),
        'page_end': page_end,
        'page': page,
        'per_page': per_page,
        'pager': pager,
        'offsets': offsets,
        'count': count,
        'bookmarks': bookmarks,
        'base_url': base_url,
        'current_tags': current_tags,
        'tags': [Tag.objects.get(slug=t) for t in tags],
        'session': session,
        'folders': folders,
        'parent_folder': parent_folder,
    })
    return HttpResponse(template.render(context))

@login_required()
def sessions(request):
    if 'removetag' in request.GET and 'tags' in request.GET :
        current_tags = request.GET['tags']
        tags = current_tags.split(",");
        tags = [tag for tag in tags if tag != request.GET['removetag']]
        current_tags = ",".join(tags);
    elif 'tags' in request.GET:
        current_tags = request.GET['tags']
        tags = current_tags.split(",")
    else:
        current_tags = ''
        tags = []

    hidden_tags = ["hidden"]
    hidden_tags = [hidden_tag for hidden_tag in hidden_tags if hidden_tag not in tags]
    sessions = Session.objects.exclude(tags__slug__in=hidden_tags)

    base_url = request.get_host()

    if tags:
        for tag in tags:
            sessions = sessions.filter(tags__slug=tag)

    count = sessions.count()

    if 'page' in request.GET and int(request.GET['page']) >= 1:
        page = int(request.GET['page'])
    else:
        page = 1
    if 'per_page' in request.GET and int(request.GET['per_page']) >= 1:
        per_page = int(request.GET['per_page'])
    else:
        per_page = 10

    page_start = ((page - 1) * per_page)
    page_end = min( page * per_page, count)

    sessions = sessions.order_by('-datetime_added')[page_start:page_end]

    pager_arg_for_tags = "&tags="+current_tags if tags else ""
    num_pages = int(((count-1)/per_page) + 1)

    #pager = map((lambda x: ("<a href=\"" + reverse('bookmarkmanager:sessions') + "?page=" + str(x) + "&per_page=" + str(per_page) + pager_arg_for_tags + "\">"+str(x)+"</a>") if x != page else str(x) ),list(range(1, int((count-1)/per_page) + 2, 1)))

    """
    pager_first = ["<a href=\"" + reverse('bookmarkmanager:sessions') + "?page=1&per_page=" + str(per_page) + pager_arg_for_tags + "\"><<</a>"] if page != 1 else []
    pager_prev = ["<a href=\"" + reverse('bookmarkmanager:sessions') + "?page="+str(page-1)+"&per_page=" + str(per_page) + pager_arg_for_tags + "\"><</a>"] if page > 2 else [] 
    pager_current = ["page "+str(page)+" of "+str(num_pages)]
    pager_next = ["<a href=\"" + reverse('bookmarkmanager:sessions') + "?page="+str(page+1)+"&per_page=" + str(per_page) + pager_arg_for_tags + "\">></a>"] if page < num_pages - 1 else []
    pager_last = ["<a href=\"" + reverse('bookmarkmanager:sessions') + "?page="+str(num_pages)+"&per_page=" + str(per_page) + pager_arg_for_tags + "\">>></a>"] if page < num_pages else []

    pager = pager_first + pager_prev + pager_next + pager_last
    """

    pager_first = ["<a href=\"" + reverse('bookmarkmanager:sessions') + "?page=1&per_page=" + str(per_page) + pager_arg_for_tags + "\">1</a>"] if page != 1 else []
    pager_second = ["<a href=\"" + reverse('bookmarkmanager:sessions') + "?page=2&per_page=" + str(per_page) + pager_arg_for_tags + "\">2</a>"] if page > 3 else []
    pager_dotsbefore = [".."] if page > 4 else [] 
    pager_prev = ["<a href=\"" + reverse('bookmarkmanager:sessions') + "?page="+str(page-1)+"&per_page=" + str(per_page) + pager_arg_for_tags + "\">"+str(page-1)+"</a>"] if page > 2 else [] 
    pager_current = [str(page)]
    pager_next = ["<a href=\"" + reverse('bookmarkmanager:sessions') + "?page="+str(page+1)+"&per_page=" + str(per_page) + pager_arg_for_tags + "\">"+str(page+1)+"</a>"] if page < num_pages - 1 else []
    pager_dotsafter = [".."] if page < num_pages - 3 else [] 
    pager_secondlast = ["<a href=\"" + reverse('bookmarkmanager:sessions') + "?page="+str(num_pages-1)+"&per_page=" + str(per_page) + pager_arg_for_tags + "\">"+str(num_pages-1)+"</a>"] if page < num_pages - 2  else []
    pager_last = ["<a href=\"" + reverse('bookmarkmanager:sessions') + "?page="+str(num_pages)+"&per_page=" + str(per_page) + pager_arg_for_tags + "\">"+str(num_pages)+"</a>"] if page < num_pages else []

    pager = pager_first + pager_second + pager_dotsbefore + pager_prev + pager_current + pager_next + pager_dotsafter + pager_secondlast +  pager_last


    offsets = map((lambda x: ("<a href=\"" + reverse('bookmarkmanager:sessions') + "?page=1&per_page=" + str(x) + pager_arg_for_tags + "\">"+str(x)+"</a>") if x != per_page else str(x) ),[10,25,50,100])

    #if (page_end > count):
    #    page_end = count

    template = loader.get_template('bookmarkmanager/sessions.html')
    context = RequestContext(request, {
        'page_start': (page_start + 1),
        'page_end': page_end,
        'page': page,
        'per_page': per_page,
        'pager': pager,
        'num_pages': num_pages,
        'offsets': offsets,
        'count': count,
        'sessions': sessions,
        'base_url': base_url,
        'current_tags': current_tags,
        'tags': [Tag.objects.get(slug=t) for t in tags],
    })
    return HttpResponse(template.render(context))

@login_required()
def tags(request):

    hidden_tags = ["hidden"]

    tags = SessionsSaverTag \
               .objects.exclude(slug__in=hidden_tags) \
               .filter(taggit_taggeditem_items__gt = 0) \
               .distinct() \
               .order_by('name') 

    #count = tags.count()

    #sort by number of sessions, then number of bookmarks
    def tag_sort_key(tag):
        return (tag.sessions(hidden_tags=hidden_tags).count(),tag.bookmarks(hidden_tags=hidden_tags).count())

    tags = sorted(tags, key=tag_sort_key, reverse=True)
  
    # filter out tags that are only linked to hidden bookmarks or sessions
    # attach session and bookmark info (that takes hidden tags into account) to each tag
    tag_info = [ \
                   [ \
                       tag.name, \
                       tag.slug, \
                       tag.sessions(hidden_tags=hidden_tags).count(), \
                       tag.slug, \
                       tag.bookmarks(hidden_tags=hidden_tags).count(), \
                   ] \
                   for tag in tags \
                   if tag.sessions(hidden_tags=hidden_tags).count() > 0 \
                   or tag.bookmarks(hidden_tags=hidden_tags).count() > 0 \
               ]    

    count = len(tag_info)
 
    #           sort by total tagged items
    #           .annotate(Count('taggit_taggeditem_items')) \
    #           .order_by('-taggit_taggeditem_items__count')

    #           this doesn't work
    #           .annotate(num_sessions=Count('session'),num_bookmarks=Count('bookmark')) \
    #           .order_by('-num_sessions', '-num_bookmarks')

    template = loader.get_template('bookmarkmanager/tags.html')
    context = RequestContext(request, {
        'taginfo': tag_info,
        'count': count,
        'tags': tags,
    })
    return HttpResponse(template.render(context))


@login_required()
def add_bookmark(request):
    if request.GET :
        form = BookmarkForm(request.GET)
        if "url" in request.GET:
            parsed_url = urlparse.urlparse(request.GET["url"])

            url_pathonly = parsed_url[0]+"://"+parsed_url[1]+parsed_url[2]
            filtered_bookmarks = Bookmark.objects.filter(url__startswith=url_pathonly)
            prev_submission_count_pathonly = filtered_bookmarks.count()

            url_minusf = url_pathonly + "?" + parsed_url[4] if parsed_url[4] else url_pathonly
            filtered_bookmarks = filtered_bookmarks.filter(url__startswith=url_minusf)
            prev_submission_count_withq = filtered_bookmarks.count()

            filtered_bookmarks = filtered_bookmarks.filter(url__startswith=request.GET["url"])
            prev_submission_count_withqandf = filtered_bookmarks.count()
            prev_submission_count = prev_submission_count_pathonly
            context_args = {
                'prev_submission_count': prev_submission_count if prev_submission_count else 0,
                'prev_submission_count_pathonly': prev_submission_count_pathonly if prev_submission_count else 0,
                'prev_submission_count_withq': prev_submission_count_withq if prev_submission_count else 0,
                'prev_submission_count_withqandf': prev_submission_count_withqandf if prev_submission_count else 0,
            }
        else:
            context_args = {}
    else:
        form = BookmarkForm()
        context_args = {}
    template = loader.get_template('bookmarkmanager/add_bookmark.html')
    context_args.update({
        'form': form,
        'error': False,
        'next': None,
    })
    context = RequestContext(request, context_args)
    return HttpResponse(template.render(context))

@login_required()
def submit_bookmark(request):
    if "cancel" in request.POST:
        return HttpResponseRedirect(reverse('bookmarkmanager:index'))
    else:
        form = BookmarkForm(request.POST)

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect(reverse('bookmarkmanager:view_bookmark', args=(form.instance.pk,)))
        else:
            template = loader.get_template('bookmarkmanager/add_bookmark.html')
            context = RequestContext(request, {
                'form': form,
                'error': True,
                'next': None,
            })
            return HttpResponse(template.render(context))

@login_required()
def update_bookmark(request,bookmark_id):
    if "cancel" in request.POST:
        return HttpResponseRedirect(reverse('bookmarkmanager:index'))
    else:
        bookmark = Bookmark.objects.get(id=bookmark_id)
        form = BookmarkForm(request.POST, instance=bookmark)

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect(request.POST.get('next'))
            #return HttpResponseRedirect(reverse('bookmarkmanager:view_bookmark', args=(form.instance.pk,)))
        else:
            template = loader.get_template('bookmarkmanager/add_bookmark.html')
            context = RequestContext(request, {
                'form': form,
                'error': True,
                'next': request.POST.get('next'),
            })
            return HttpResponse(template.render(context))

@login_required()
def edit_bookmark(request,bookmark_id):
    bookmark = Bookmark.objects.get(id=bookmark_id)
    form = BookmarkForm(instance=bookmark)
    template = loader.get_template('bookmarkmanager/add_bookmark.html')
    context = RequestContext(request, {
        'bookmark_id'  : bookmark_id,
        'owner' : bookmark.owner.username,
        'form': form,
        'next': request.META['HTTP_REFERER'],
    })
    return HttpResponse(template.render(context))

@login_required()
def view_bookmark(request,bookmark_id):
    base_url = request.get_host()
    try:
        bookmark = Bookmark.objects.get(id=bookmark_id)
    except Bookmark.DoesNotExist:
        bookmark = None
    template = loader.get_template('bookmarkmanager/bookmark.html')
    context = RequestContext(request, {
        'bookmark'  : bookmark,
        'owner' : bookmark.owner.username if bookmark else None,
        'base_url': base_url,
    })
    return HttpResponse(template.render(context))


@login_required()
def delete_bookmark(request,bookmark_id):
    bookmark = Bookmark.objects.get(id=bookmark_id)
    bookmark.delete()
    return HttpResponseRedirect(request.META['HTTP_REFERER'])
    #return HttpResponseRedirect(reverse('bookmarkmanager:index'))

@login_required()
def add_session(request):
    if request.GET :
        form = SessionForm(request.GET)
    else:
        form = SessionForm()
    template = loader.get_template('bookmarkmanager/edit_session.html')
    context = RequestContext(request, {
        'form': form,
        'error': False,
        'next': None,
    })
    return HttpResponse(template.render(context))

@login_required()
def submit_session(request):
    if "cancel" in request.POST:
        return HttpResponseRedirect(reverse('bookmarkmanager:sessions'))
    else:
        form = SessionForm(request.POST)

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect(reverse('bookmarkmanager:sessions'))
        else:
            template = loader.get_template('bookmarkmanager/edit_session.html')
            context = RequestContext(request, {
                'form': form,
                'error': True,
                'next': None,
            })
            return HttpResponse(template.render(context))

@login_required()
def update_session(request,session_id):
    if "cancel" in request.POST:
        return HttpResponseRedirect(reverse('bookmarkmanager:sessions'))
    else:
        session = Session.objects.get(id=session_id)
        form = SessionForm(request.POST, instance=session)

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect(request.POST.get('next'))
            #return HttpResponseRedirect(reverse('bookmarkmanager:sessions'))
        else:
            template = loader.get_template('bookmarkmanager/edit_session.html')
            context = RequestContext(request, {
                'form': form,
                'error': True,
                'next': request.POST.get('next'),
            })
            return HttpResponse(template.render(context))

@login_required()
def edit_session(request,session_id):
    session = Session.objects.get(id=session_id)
    form = SessionForm(instance=session)
    template = loader.get_template('bookmarkmanager/edit_session.html')
    context = RequestContext(request, {
        'session_id'  : session_id,
        'owner' : session.owner.username,
        'form': form,
        'next': request.META['HTTP_REFERER'],
    })
    return HttpResponse(template.render(context))

@login_required()
def delete_session(request,session_id):
    session = Session.objects.get(id=session_id)
    session.delete()
    #return HttpResponseRedirect(request.META['HTTP_REFERER'])
    return HttpResponseRedirect(reverse('bookmarkmanager:sessions'))

@login_required()
def add_folder(request):
    if request.GET :
        form = FolderForm(request.GET)
    else:
        form = FolderForm()
    template = loader.get_template('bookmarkmanager/forms/folder.html')
    context = RequestContext(request, {
        'form': form,
        'error': False,
        'next': None,
    })
    return HttpResponse(template.render(context))

@login_required()
def submit_folder(request):
    if "cancel" in request.POST:
        return HttpResponseRedirect(request.POST.get('next'))
        #return HttpResponseRedirect(reverse('bookmarkmanager:index')+"?f=0")
    else:
        form = FolderForm(request.POST)

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            # redirect to new URL:
            return HttpResponseRedirect(reverse('bookmarkmanager:index')+"?f="+str(form.instance.pk))
        else:
            template = loader.get_template('bookmarkmanager/forms/folder.html')
            context = RequestContext(request, {
                'form': form,
                'error': True,
                'next': None,
            })
            return HttpResponse(template.render(context))

@login_required()
def update_folder(request,folder_id):
    if "cancel" in request.POST:
        return HttpResponseRedirect(reverse('bookmarkmanager:index')+"?f=0")
    else:
        folder = Folder.objects.get(id=folder_id)
        form = FolderForm(request.POST, instance=folder)

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect(request.POST.get('next'))
            #return HttpResponseRedirect(reverse('bookmarkmanager:index')+"?f=0")
        else:
            template = loader.get_template('bookmarkmanager/forms/folder.html')
            context = RequestContext(request, {
                'form': form,
                'error': True,
                'next': request.POST.get('next'),
            })
            return HttpResponse(template.render(context))

@login_required()
def edit_folder(request,folder_id):
    folder = Folder.objects.get(id=folder_id)
    form = FolderForm(instance=folder)
    template = loader.get_template('bookmarkmanager/forms/folder.html')
    context = RequestContext(request, {
        'folder_id'  : folder_id,
        'owner' : folder.owner.username,
        'form': form,
        'next': request.META['HTTP_REFERER'],
    })
    return HttpResponse(template.render(context))

@login_required()
def delete_folder(request,folder_id):
    folder = Folder.objects.get(id=folder_id)
    if folder.subfolders.count() == 0 and folder.bookmarks.count() == 0:
        folder.delete()
    return HttpResponseRedirect(request.META['HTTP_REFERER'])
    #return HttpResponseRedirect(reverse('bookmarkmanager:index')+"?f=0")



@login_required()
def bm_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('bookmarkmanager:index'))
