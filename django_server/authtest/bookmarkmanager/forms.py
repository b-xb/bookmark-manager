from django.forms import ModelForm
from sessionssaver.models import Bookmark, Session, Folder, TestModel

# Create the form class.
class BookmarkForm(ModelForm):
     class Meta:
         model = Bookmark

class SessionForm(ModelForm):
     class Meta:
         model = Session

class FolderForm(ModelForm):
     class Meta:
         model = Folder

class TestModelForm(ModelForm):
     class Meta:
         model = TestModel
