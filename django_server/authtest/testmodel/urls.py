from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from testmodel import views

urlpatterns = [
    url(r'^$', views.submit_form),
]

urlpatterns = format_suffix_patterns(urlpatterns)
