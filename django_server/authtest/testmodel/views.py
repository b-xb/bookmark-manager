#from django.shortcuts import render

# Create your views here.

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from testmodel.models import TestModel
from testmodel.serializers import TestModelSerializer

@api_view(['POST','GET'])
@authentication_classes((SessionAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def submit_form(request):
    """
    submit the form 
    """
    if request.method == 'GET':
        things = TestModel.objects.all()
        serializer = TestModelSerializer(things, many=True)
        response = Response(serializer.data)
        response['Access-Control-Allow-Origin'] = "chrome-extension://fpdajomjcmepdalndecaefggleinnpil"
        return response
 
    if request.method == 'POST':
        serializer = TestModelSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
