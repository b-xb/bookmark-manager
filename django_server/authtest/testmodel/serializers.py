from rest_framework import serializers
from testmodel.models import TestModel


class TestModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestModel
        fields = ('pk','thingy')
