# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TestModel'
        db.create_table(u'testmodel_testmodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('thingy', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'testmodel', ['TestModel'])


    def backwards(self, orm):
        # Deleting model 'TestModel'
        db.delete_table(u'testmodel_testmodel')


    models = {
        u'testmodel.testmodel': {
            'Meta': {'object_name': 'TestModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'thingy': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['testmodel']